
$('.app-box').on('click', function(){
    $('.app-box.current').removeClass('current');
    $(this).addClass('current');
    $('.app-box.current').siblings().css({"display":"none"});
    $('.app-box.current .app-content').css({"display":"block"});
});



$( ".app-box" ).each(function(index) {
    $('.app-box').on('click', function(){
        $('.back-supp').css({"opacity":"1", left:"0", "transition":"all .5s", "display":"block"});
    });
});
$( ".carousel-indicators .active" ).each(function(index) {
    $('.carousel-indicators .active').on('click', function(){
        $('.app-box').removeClass('current');
        $('.app-box').css({"display":"block"});
        $('.app-box .app-content').css({"display":"none"});
        $('.back-supp').css({"display":"none"});
    });
});
$('.carousel-indicators button').on('click', function(){
    $('.back-supp').css({"display":"none"});
});
$('.back-supp').on('click', function(){
    $(this).hide();
    $('.app-box').removeClass('current');
    $('.app-box .app-content').css({"display":"none"});
    $('.app-box').css({"display":"block"});
});



setRandomClass();
setInterval(function () {
    setRandomClass();
}, 3000);

function setRandomClass() {
    var ul = $(".partner-boxes");
    var items = ul.find(".canvas-box");
    var number = items.length;
    var random = Math.floor((Math.random() * number));
    items.removeClass("special");
    items.eq(random).addClass("special");
}
// Random Class Add Remove


//Scroll Header Sticky
$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $("#main-header").addClass("header-sticky");
    }
    else{
        $("#main-header").removeClass("header-sticky");
    }
   
});
//Scroll Header Sticky



// Scroll Locamotive Animation Js
if($(window).width() <= 768){
const scroller = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true
})

gsap.registerPlugin(ScrollTrigger)
scroller.on('scroll', ScrollTrigger.update)
ScrollTrigger.scrollerProxy(
    '.container-set', {
        scrollTop(value) {
            return arguments.length ?
            scroller.scrollTo(value, 0, 0) :
            scroller.scroll.instance.scroll.y
        },
        getBoundingClientRect() {
            return {
                left: 0, top: 0, 
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }
)

ScrollTrigger.addEventListener('refresh', () => scroller.update())
ScrollTrigger.refresh()  
}


// Scroll Locamotive Animation Js


$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}



if($(window).width() <= 415){
    if(('.hu-it-serv').length != 0){
        $('.hu-it-serv').addClass('owl-carousel owl-theme');
        $('.hu-it-serv').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                }, 
                413:{
                    
                    items:2,
                    nav:true,
                    dots: false,
                }
                
            }
        });
    }
}


$('#nav-icon3').click(function(){
    $(this).toggleClass('open');
});


// Services Line Trigger
gsap.from(".line-move", {
    duration: 0.5,
    xPercent: 0
});
var FirstChild = $(".carousel-indicators .col-lg-4:nth-child(1) button");
var SecChild = $(".carousel-indicators .col-lg-4:nth-child(2) button");
var ThridChild = $(".carousel-indicators .col-lg-4:nth-child(3) button");
FirstChild.click(() => {
    gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 0
    });
});
SecChild.click(() => {
      gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 100
      });
    //   $('.container-set').css({"transform":"matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, -4694, 0, 1)"});
});
ThridChild.click(() => {
    gsap.to(".line-move", {
      duration: 0.5,
      xPercent: 200
    });
});
// Services Line Trigger



