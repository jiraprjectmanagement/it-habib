




$(".dropdown-menu").mouseenter(function(){
  $(this).parent().addClass('hvr-active');
});
$(".dropdown-menu").mouseleave(function(){
    $(this).parent().removeClass('hvr-active');
  });

$('.app-box').on('click', function(){
    $('.app-box.current').removeClass('current');
    $(this).addClass('current'); 
    $('.app-box.current').siblings().css({"display":"none"});
    $('.app-box.current .app-content').css({"display":"block"});
});
$('.app-box').on('click', function(){
    $(".app-content.appcontent-scroll").removeClass('appcontent-scroll');
    $(".app-box .app-content").addClass('appcontent-scroll');

});





$( ".app-box" ).each(function(index) {
    $('.app-box').on('click', function(){
        $('.back-supp').css({"opacity":"1", left:"0", "transition":"all .5s", "display":"block"});
    });
});
// $( ".carousel-indicators .active" ).each(function(index) {
    $('.carousel-indicators .active').on('click', function(){
        $('.app-box').removeClass('current');
        $('.app-box').css({"display":"block"});
        $('.app-box .app-content').css({"display":"none"});
    });
// });


$('.back-supp').on('click', function(){
    $(this).hide();
    $('.app-box').removeClass('current');
    $('.app-box .app-content').css({"display":"none"});
    $('.app-box').css({"display":"block"});
    // scrollSlimRemove();

});

$('.carousel-indicators button').on('click', function(){
    $('.back-supp').css({"display":"none"});
});


//Scroll Header Sticky

//Scroll Header Sticky


$(".opener").click(function (event) {
    if ($('.hamburger').hasClass("is-active")) {
        $('.hamburger').removeClass("is-active");
        $('.hamburger-label').text('Menu');
        $('#navigation').css('top', '-100%').addClass('visible');
        $('body').find('#nav-socials').insertAfter('#navigation').css({
            'position': 'fixed',
            'top': '50%',

        });
    }
    event.preventDefault();

    let link = $(this).attr('title');
    $("html").removeClass('has-scroll-smooth');
    $("body").addClass('overflow-x');
    $(".c-scrollbar").css('visibility', 'hidden');
    $("#dialog").load(link).dialog("open");

});





$('.projects-area-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
        },                   
        574:{
            items:2,
            dots: true,
            nav: false,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}


if($(window).width() <= 575){
    if(('.tech-unit-row').length != 0){
        $('.tech-unit-row').addClass('owl-carousel owl-theme');
        $('.tech-unit-row').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}


if($(window).width() <= 768){
    if(('.hu-it-serv').length != 0){
        $('.hu-it-serv').addClass('owl-carousel owl-theme');
        $('.hu-it-serv').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                413:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                }
                
            }
        });
    }
}


if($(window).width() <= 768){
    if(('.partner-boxes').length != 0){
        $('.partner-boxes').addClass('owl-carousel owl-theme');
        $('.partner-boxes').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                    margin:0,
                    stagePadding: 10,
                },
                575:{

                    items:2,
                    nav:true,
                    dots: false,
                    margin:10,
                }
                
            }
        });
    }
    $(".partner-boxes .owl-item:eq(0)").css({"order":"12", "border-color":"transparent"});
}

// Menu Bar

$(".menu-inner button").click(function() {
    var BtnAttr = $(this).attr("aria-expanded");
      if(BtnAttr == 'false'){
          $('#nav-icon3').removeClass("open");
      }
      if(BtnAttr == 'true'){
        $('#nav-icon3').addClass("open");
    }
 });
// Menu Bar


// var tl = new TimelineMax({onUpdate:updatePercentage});
// var controller = new ScrollMagic.Controller();

// // tl.from('.s-2-inner', 1, {y: -300, opacity: 0});
// // tl.from('.s-2-inner .yea-exp', 1, {duration: 3, rotation: 120});
// tl.from('.s-2-inner .small-cirlc', 1, {duration: 5, rotation: 90});
// tl.from('.s-2-inner .bg-circle', 1, {duration: 5, y: 50});
// tl.from('.sec-heading', 1, {x: 30, opacity: 0});
// // tl.from('.s-2-inner a', 1, {x: 100, opacity: 0});
// // tl.from('.s-2 img', 1, {x: 50, opacity: 0});
// // tl.from('.s-2 .box', 1, {scale: 0, opacity: 0}, "-=2");


// const scene = new ScrollMagic.Scene({
//   triggerElement: ".s-2",
//   triggerHook: "onLeave",
//   duration: "100%",
// })
//   .setPin(".s-2")
//   .setTween(tl)
//     .addTo(controller);

// function updatePercentage() {
//   tl.progress();
//   console.log(tl.progress());
// }





  
var tl = new TimelineMax({onUpdate:updatePercentage});
var controller = new ScrollMagic.Controller();

tl.from('#OurObjectives .small-cirlc', 1, {rotation: 50});
tl.from('#OurObjectives .bg-circle', 1, {y: 50});


const scene = new ScrollMagic.Scene({
  triggerElement: "#OurObjectives",
  triggerHook: "onLeave",
  duration: "100%",
})
//   .setPin("#OurObjectives")
  .setTween(tl)
    .addTo(controller);

function updatePercentage() {
  tl.progress();
  console.log(tl.progress());
}


  
var tl1 = new TimelineMax({onUpdate:updatePercentage1});
var controller1 = new ScrollMagic.Controller();

tl1.from('#TechPartners .sec-heading', 1, {x: -40});
tl1.to('#TechPartners .sec-heading', 1, {x: 0});
tl1.from('#TechPartners h2', 1, {x: 40});
tl1.to('#TechPartners h2', 1, {x: 0});
tl1.from('#TechPartners .small-cirlc', 1, {rotation: 30});
tl1.from('#TechPartners .small-cirlc', 1, {rotation: 70});

const scene1 = new ScrollMagic.Scene({
  triggerElement: "#TechPartners",
  triggerHook: "onLeave",
  duration: "100%",
})
//   .setPin("#TechPartners")
  .setTween(tl1)
    .addTo(controller1);

function updatePercentage1() {
  tl1.progress();
  console.log(tl1.progress());
}





var tl2 = new TimelineMax({onUpdate:updatePercentage2});
var controller2 = new ScrollMagic.Controller();

tl2.from('#ITServices .s2-shape', 1, {y: 40});
tl2.to('#ITServices .s2-shape', 1, {y: 0});

tl2.from('#ITServices .s1-shape', 1, {y: -40});
tl2.to('#ITServices .s1-shape', 1, {y: 0});

const scene2 = new ScrollMagic.Scene({
  triggerElement: "#ITServices",
  triggerHook: "onLeave",
  duration: "100%",
})
//   .setPin("#ITServices")
  .setTween(tl2)
    .addTo(controller2);

function updatePercentage2() {
  tl2.progress();
  console.log(tl2.progress());
}











