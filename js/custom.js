



// $(".back-supp").click(function(){
//   $(".app-box").attr("class","");

// });
$( ".app-box" ).each(function(index) {
    $('.app-box').on('click', function(){
        $('.back-supp').css({"opacity":"1", "transition":"all .5s", "display":"table"});
    });
});

$(".dropdown-menu").mouseenter(function(){
  $(this).parent().addClass('hvr-active');
});
$(".dropdown-menu").mouseleave(function(){
    $(this).parent().removeClass('hvr-active');
  });

$('.app-box').on('click', function(){
    $('.app-box.current').removeClass('current');
    $(this).addClass('current'); 
    $('.app-box.current').siblings().css({"display":"none"});
    $('.app-box.current .app-content').css({"display":"block"});
});
// $('.back-supp').on('click', function(){
//     $(".app-box").removeClass("current")
//     $('.app-box').siblings().css({"display":"block"});
// });
$('.app-box').on('click', function(){
    $(".app-content.appcontent-scroll").removeClass('appcontent-scroll');
    $(".app-box .app-content").addClass('appcontent-scroll');

});





// $( ".app-box" ).each(function(index) {
//     $('.app-box').on('click', function(){
//         $('.back-supp').css({"opacity":"1", left:"0", "transition":"all .5s", "display":"block"});
//     });
// });
// $( ".carousel-indicators .active" ).each(function(index) {
    $('.carousel-indicators .active').on('click', function(){
        $('.app-box').removeClass('current');
        $('.app-box').css({"display":"block"});
        $('.app-box .app-content').css({"display":"none"});
    });
// });


$('.back-supp').on('click', function(){
    $(this).hide();
    $('.app-box').removeClass('current');
    $('.app-box .app-content').css({"display":"none"});
    $('.app-box').css({"display":"block"});
    // scrollSlimRemove();
   
});

$('.carousel-indicators button').on('click', function(){
    $('.back-supp').css({"display":"none"});
});


//Scroll Header Sticky

//Scroll Header Sticky


// $(".opener").click(function (event) {
//     if ($('.hamburger').hasClass("is-active")) {
//         $('.hamburger').removeClass("is-active");
//         $('.hamburger-label').text('Menu');
//         $('#navigation').css('top', '-100%').addClass('visible');
//         $('body').find('#nav-socials').insertAfter('#navigation').css({
//             'position': 'fixed',
//             'top': '50%',

//         });
//     }
//     event.preventDefault();

//     let link = $(this).attr('title');
//     $("html").removeClass('has-scroll-smooth');
//     $("body").addClass('overflow-x');
//     $(".c-scrollbar").css('visibility', 'hidden');
//     $("#dialog").load(link).dialog("open");

// });




$('.projects-area-slider').owlCarousel({
    loop:true,
    nav: true,
    navText: ['<img src="img/left-icon.svg" alt="">', '<img src="img/right-icon.svg" alt="">'],
    dots: true,
    margin:25,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: true,
            nav: false,
            margin:10,
            stagePadding: 30,
        },                   
        574:{
            items:1,
            dots: true,
            nav: false,
            margin:10,
            stagePadding: 30,
        },
        766: {
            items: 2,  
            dots: true,
            nav: false,
            margin:10,
        }, 
        990: {
            items: 3,
            dots: true,
            nav: true,
        }, 
        1199: {
            items: 4,
            dots: true,
            nav: true,
        }
    }
});




if($(window).width() <= 768){
    if(('.projects-box-slider').length != 0){
        $('.projects-box-slider').addClass('owl-carousel owl-theme');
        $('.projects-box-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                }, 
                479:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                574:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                },
                767:{
                    items:2,
                    margin:10,
                }
                
            }
        });
    }
}


if($(window).width() <= 575){
    if(('.tech-unit-row').length != 0){
        $('.tech-unit-row').addClass('owl-carousel owl-theme');
        $('.tech-unit-row').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                    margin:10,
                }, 
                480:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                    margin:10,
                },
                577:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                    margin:10,
                },
                767:{
                    items:1,
                }
                
            }
        });
    }
}


if($(window).width() <= 768){
    if(('.hu-it-serv').length != 0){
        $('.hu-it-serv').addClass('owl-carousel owl-theme');
        $('.hu-it-serv').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:false,
                    dots: true,
                    margin:50,
                }, 
                413:{
                    
                    items:1,
                    nav:false,
                    dots: true,
                    margin:50,
                }
                
            }
        });
    }
}


if($(window).width() <= 768){
    if(('.partner-boxes').length != 0){
        $('.partner-boxes').addClass('owl-carousel owl-theme');
        $('.partner-boxes').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    nav:true,
                    dots: false,
                    margin:0,
                    stagePadding: 10,
                },
                575:{

                    items:1,
                    nav:true,
                    dots: false,
                    margin:10,
                },
                768:{

                    items:2,
                    nav:true,
                    dots: false,
                    margin:10,
                }
                
            }
        });
    }
    $(".partner-boxes .owl-item:eq(0)").css({"order":"12", "border-color":"transparent"});
}

// Menu Bar

$(".collapse-btn").click(function() {
    var BtnAttr = $(this).attr("aria-expanded");
      if(BtnAttr == 'false'){
          $('.nav-icon3').removeClass("open");
      }
      if(BtnAttr == 'true'){
        $('.nav-icon3').addClass("open");
    }
 });
// Menu Bar





// Scroll Locamotive Animation Js


// const myTimeout = setTimeout(TimeOutScroll, 3000);
// function TimeOutScroll() {
//     const scroller = new LocomotiveScroll({
//         el: document.querySelector('.smooth-scroll'),
//         smooth: true,
//             mobile: {
//              smooth: false
//             },
//             tablet: {
//                 smooth: false
//             }
//     })
// }
// gsap.registerPlugin(ScrollTrigger)
// scroller.on('scroll', ScrollTrigger.update)
// ScrollTrigger.scrollerProxy(
//     '.container-set', {
//         scrollTop(value) {
//             return arguments.length ?
//             scroller.scrollTo(value, 0, 0) :
//             scroller.scroll.instance.scroll.y
//         },
//         getBoundingClientRect() {
//             return {
//                 left: 0, top: 0, 
//                 width: window.innerWidth,
//                 height: window.innerHeight
//             }
//         }
//     }
// )
// ScrollTrigger.addEventListener('refresh', () => scroller.update())
// ScrollTrigger.refresh()




// console.clear();

// gsap.registerPlugin(ScrollTrigger);

// const locoScroll = new LocomotiveScroll({
//   el: document.querySelector("[data-scroll-container]"),
//   smooth: true,
//     mobile: {
//      smooth: false
//     },
//     tablet: {
//         smooth: false
//     }
// });

// locoScroll.on("scroll", ScrollTrigger.update);


// ScrollTrigger.scrollerProxy("[data-scroll-container]", {
//   scrollTop(value) {
//     return arguments.length ? locoScroll.scrollTo(value, 0, 0) : locoScroll.scroll.instance.scroll.y;
//   },
//   getBoundingClientRect() {
//     return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
//   },
//   pinType: document.querySelector("[data-scroll-container]").style.transform ? "transform" : "fixed"
// });


// ScrollTrigger.addEventListener("refresh", () => locoScroll.update());

//      ScrollTrigger.refresh();
  



  const pageContainer = document.querySelector("[data-scroll-container]");

  const scroll = new LocomotiveScroll({
    el: pageContainer,
    smooth: true,
  });
  
  // Add remove Class On scroll With Locomotive
    scroll.on('scroll', (position) => {
      if ((position.scroll.y) > 50) {
        document.querySelector('#main-header').classList.add('scrolled');
      } else {
        document.querySelector('#main-header').classList.remove('scrolled');
      }
    });
  // Add remove Class On scroll With Locomotive


  scroll.on(pageContainer, ScrollTrigger.update);

  ScrollTrigger.scrollerProxy(pageContainer, {
    scrollTop(value) {
      return arguments.length ? scroll.scrollTo(value, 0, 0) : scroll.scroll.instance.scroll.y;
    },
    getBoundingClientRect() {
      return {
        top: 0,
        left: 0,
        width: window.innerWidth,
        height: window.innerHeight
      };
    }
  });

  /* anims */

//   scroll.on('call', func => {
//     console.log("block1 triggered");
// });
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll <= 50) {
        $(".responsive-menu-bar button").removeClass("responsive-Header-sticky");
    }
    if (scroll >= 50) {
      $(".responsive-menu-bar button").addClass("responsive-Header-sticky");
  }
});

  function animateFrom(elem, direction) {
    direction = direction || 1;

    var x = 0,
      y = direction * 100;
    if (elem.classList.contains("gsap_reveal--fromLeft")) {
      x = -100;
      y = 0;
    } else if (elem.classList.contains("gsap_reveal--fromRight")) {
      x = 100;
      y = 0;
    }

    elem.style.transform = "translate(" + x + "px, " + y + "px)";
    elem.style.opacity = "0";

    gsap.fromTo(elem, {
      x: x,
      y: y,
      autoAlpha: 0
    }, {
      duration: 2,
      x: 0,
      y: 0,
      autoAlpha: 1,
      ease: "expo",
      overwrite: "auto",
    });

  }

  function hide(elem) {
    gsap.set(elem, {
      autoAlpha: 0
    });
  }

  gsap.utils.toArray(".gsap_reveal").forEach(function(elem) {
    hide(elem); // assure that the element is hidden when scrolled into view
    ScrollTrigger.create({
      trigger: elem,
      scroller: "[data-scroll-container]",
      onEnter: function() {
        animateFrom(elem)
      },
      onEnterBack: function() {
        animateFrom(elem, -1)
      },
      once: true,
    });
  });


  /* anims end */

  window.addEventListener("load", function(event) {
    ScrollTrigger.refresh();
  });


  ScrollTrigger.addEventListener("refresh", () => scroll.update());
  ScrollTrigger.refresh();


 
// // Scroll Locamotive Animation Js






// Services Line Trigger
gsap.from(".line-move", {
    duration: 0.5,
    xPercent: 0
});
var FirstChild = $(".carousel-indicators .col-lg-4:nth-child(1) button");
var SecChild = $(".carousel-indicators .col-lg-4:nth-child(2) button");
var ThridChild = $(".carousel-indicators .col-lg-4:nth-child(3) button");
FirstChild.click(() => {
    gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 0
    });
});
SecChild.click(() => {
      gsap.to(".line-move", {
        duration: 0.5,
        xPercent: 100
      });
});
ThridChild.click(() => {
    gsap.to(".line-move", {
      duration: 0.5,
      xPercent: 200
    });
});
// Services Line Trigger




// Banner Line Animation
var svgEl = document.querySelector('.animated-lines');
var randomRange = function(min, max) {
  return ~~(Math.random() * (max - min + 1)) + min
};
var numberOfLines = 10,
  lineDataArr = [];
var createPathString = function() {
  var completedPath = '',
    comma = ',',
    ampl = 5; // pixel range from 0, aka how deeply they bend
  for (var i = 0; i < numberOfLines; i++) {
    var path = lineDataArr[i];
    var current = {
      x: ampl * Math.sin(path.counter / path.sin),
      y: ampl * Math.cos(path.counter / path.cos)
    };
    var newPathSection = 'M' +
      // starting point
      path.startX +
      comma +
      path.startY +
      // quadratic control point
      ' Q' +
      path.pointX +
      comma +
      (current.y * 1.2).toFixed(2) + // 1.5 to amp up the bend a little
      // center point intersection
      ' ' +
      ((current.x) / 10 + path.centerX).toFixed(2) +
      comma +
      ((current.y) / 5 + path.centerY).toFixed(2) +
      // end point with quadratic reflection (T) (so the bottom right mirrors the top left)
      ' T' +
      path.endX +
      comma +
      path.endY;
    path.counter++;
    completedPath += newPathSection;
  };
  return completedPath;
};
var createLines = function() {
  var newPathEl = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
    // higher is slower
    minSpeed = 85,
    maxSpeed = 150;
  // create an arr which contains objects for all lines
  // createPathString() will use this array
  for (var i = 0; i < numberOfLines; i++) {
    var lineDataObj = {
      counter: randomRange(1, 800), // a broad counter range ensures lines start at different cycles (will look more random)
      startX: randomRange(-200, -90),
      startY: randomRange(40, 70),
      endX: randomRange(400, 420), // viewbox = 200
      endY: randomRange(10, 20), // viewbox = 120
      sin: randomRange(minSpeed, maxSpeed),
      cos: randomRange(minSpeed, maxSpeed),
      pointX: randomRange(80, 60),
      centerX: randomRange(70, 170),
      centerY: randomRange(80, 50)
    }
    lineDataArr.push(lineDataObj)
  }
  var animLoop = function() {
    newPathEl.setAttribute('d', createPathString());
    requestAnimationFrame(animLoop);
  }
  // once the path elements are created, start the animation loop
  svgEl.appendChild(newPathEl);
  animLoop();
};
createLines();


// Banner Line Animation









